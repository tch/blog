<?php 

//on inclut les fichiers necessaires
include('includes/notifications.inc.php');
include('includes/connect.php');
require_once('includes/fonctions.inc.php');

//on inclut la classe Smarty
require_once('libs/Smarty.class.php');

//on créé et on instancie un objet smarty
$smarty = new Smarty();


//On vérifie si l'utilisateur existe, si oui on affecte le cookie, si non, rien
if($user)
{
$cook = $_COOKIE['email'];
}
else
{
	$cook = false;
}
$userCo = $user;


//on envois les données au template
$smarty->assign('userCo', $userCo);
$smarty->assign('cook', $cook);
$smarty->display('templates/bas.inc.tpl');



