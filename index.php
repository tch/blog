

<?php
session_start();
//on inclut les différentes parties du blog
include('includes/connect.php');
include('includes/haut.inc.php');
include('includes/notifications.inc.php');
include('includes/fonctions.inc.php');

require_once('libs/Smarty.class.php');

//TRAITEMENT ARTICLE + PAGINATION + TAGS


//On créé un objet Smarty
$smarty = new Smarty();

//Un tableau de articles pour la boucle
$articles = array();




$app=1; //Article par page

//On récupère la recherche de "r" ou "tag"
$rech = var_get('r');
$rechTa = var_get('tag');


//On sécurise 
$rechMysql = mysql_real_escape_string($rech);
$rechMysql2 = mysql_real_escape_string($rechTa);



$rechUrl = htmlspecialchars($rech);
$rechUrl2 = htmlspecialchars($rechTa);

$page=(int)var_get('p'); //on récupère la page

//si page 0 alors on l'a met à 1
if($page==0)$page=1;


//Test si la recherche tag et vide, si oui on applique la recherche normal sinon c'est une recherche avec le tag
if(empty($rechTa))
{
	$where=(isset($rech))?"WHERE texte LIKE '%$rech%' OR titre LIKE '%$rech%'":'';
}
else
{
	$where=($rechTa)?"WHERE tag LIKE '%$rechTa%'":'';
}



$debut=($page-1)*$app; //indice de départ

//On calcul le nombre d'article soit de toute la table, soit avec la recherche
$calc = "SELECT COUNT(*) AS total FROM article AS ar INNER JOIN tags ta ON ar.id=ta.id_article $where";

$res = mysql_query($calc);

//On récupère les données dans un tableau
$data = mysql_fetch_array($res);

//On affecte à total le nombre d'articles
$total= $data['total'];

//On calcul le nombre de page en fonction du total et du nombre d'article par page ($app)
$nb_pages=ceil($total/$app);

if($rech || $rechTa)
{
	$nb_pages =1;
	$app = $total;
}



//On fait une requête SQL pour récupère tous les articles et les tags avec une jointure INNER JOIN
$sql = "SELECT * FROM article AS ar INNER JOIN tags AS ta ON ar.id=ta.id_article $where GROUP BY date DESC LIMIT $debut,$app";

//Execution requête
$result = mysql_query($sql);


//Si la recherche de tag et vide, le titre de la page du blog = soit la recherche normale soit derniers articles
//Sinon le titre = soit la recherche avec le tag, ou derniers articles
if(empty($rechTa))
{
	$titre=($rech)? 'Résultat pour "'.$rech.'"':'Derniers Articles';
}
else
{
	$titre=($rechTa)? 'Tag :"'.$rechTa.'"':'Derniers Articles';
}

//On affecte diverses variables pour la pagination 
$null="$page/#null";
$suivant=$page+1;
$precedent=$page-1;
$max=$nb_pages;





//On configure la date avec le fuseau horaire GMT
date_default_timezone_set('GMT');
setlocale (LC_TIME, 'fr_FR.utf8','fra');

//On configure le format de la date selon la doc smarty
$config['date'] = '%k:%M:%S';





/* boucle qui récupère sous la forme d'un tableau dans $data les articles, si pour chaque id d'article, une image correspondante
 * existe, on affecte à l'article un nouveau champs "virtuel" image avec le nom de l'image pour pouvoir afficher dans le template
 * 
 * */

while($data = mysql_fetch_array($result))
{

	$id = $data['id'];
	$chemin = dirname(__FILE__)."/data/images/$id.jpg";



	if(file_exists($chemin))
	{
		$data['image'] = "data/images/$id.jpg";


	}
	else
	{
		$data['image'] = "";

	}

	if($data['id_article'] != $id)
	{
		$data['tag'] = "";
	}

	$articles[] = $data;




}







//TRANSMISSION AU TEMPLATE

$smarty->assign('user', $user);
$smarty->assign('titre', $titre);
$smarty->assign('max',$max);
$smarty->assign('null',$null);
$smarty->assign('precedent',$precedent);
$smarty->assign('suivant',$suivant);
$smarty->assign('page', $page);
$smarty->assign('config', $config);
$smarty->assign('article',$articles);

//affichage du template
$smarty->display('templates/index.tpl');








include('includes/bas.inc.php');






