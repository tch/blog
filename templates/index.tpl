<html>

<div class="pagination">

<ul>
	
		<li class="prev {if $page<=1} disabled {/if}"><a href="?p={if $page<=1} #null {else}{$precedent}{/if}">&larr; Precedent</a></li>
		
		{for $i=1 to $max}
 			<li {if $page==$i} class='active' {/if}>
 			<a href='?p={$i}'>{$i}</a></li>
			
		{/for}
		<li class="next {if $page>=$max} disabled {/if}"><a href="?p={if $page>=$max}{$page}{else}{$suivant}{/if}">Suivant&rarr;</a></li>
</ul>
		
</div>

<h2>{$titre}<h2>
	
{foreach from=$article item=articles}
	<h3>{$articles['titre']}</h3>	
	<p class="article">
	{if $articles['image']}
		
		<a href="data/images/{$articles['id']}.jpg"><img src="vignette.jpg.php?id={$articles['id']}"></a>
	
		
	{/if}
	
	<br/>
	<br/>
	<span>{$articles['texte']|nl2br}</span>
	<br/>
	
	<br/>
	publié le :
	{$articles['date']|date_format:"%A %d-%B-%Y"} à
	<br/>
	{$articles['date']|date_format:$config.date}
	<br/>
	
	</p>
	<div class="tag">
	Tag : <a href="?tag={$articles['tag']}">{$articles['tag']}</a>
	</div>
	<br/>
	{if $user==true}
<a href="article.php?id={$articles['id']}" class="btn btn-primary">Modifier</a>
<a href="SuppArticle.php?id={$articles['id']}" class="btn btn-danger">Supprimer</a>
	{/if}
{/foreach}

<div class="pagination">

<ul>
	
		<li class="prev {if $page<=1} disabled {/if}"><a href="?p={if $page<=1} #null {else} {$precedent}{/if}">&larr; Precedent</a></li>
		
		{for $i=1 to $max}
 			<li {if $page==$i} class='active' {/if}>
			<a href='?p={$i}'>{$i}</a></li>
		{/for}
		<li class="next {if $page>=$max} disabled {/if}"><a href="?p={if $page>=$max} {$page} {else} {$suivant}{/if}">Suivant&rarr;</a></li>
</ul>
		
</div>



</html>