<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Mon blog</title>
<meta name="description" content="Petit blog pour m'initier à PHP">
<meta name="author" content="Jean-philippe Lannoy">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/main.css" rel="stylesheet">
<style>
p.article {
	background-color: lightgrey;
	min-width: 100px;
}

span {
	font-size: 18px;
}

.tag {
	background-color: lightgrey;
}

p {
	text-align: center;
}
</style>
<script type="text/javascript" src="assets/js/jquery-1.8.3.js">


</script>
<script type="text/javascript" src="assets/js/fonction.js">
$(function(){
	$(".cacher_notif").click(function(cacherNotif));
	
});
</script>
</head>

<body>

	<div class="container">

		<div class="content">

			<div class="page-header well">
				<h1>
					Mon Blog <small>Pour m'initier à PHP</small>
				</h1>
			</div>

			<div class="row">

				<div class="span8">
					<!-- notifications -->

					<!-- contenu -->