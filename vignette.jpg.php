<?php

//on inclut les fonctions
include('includes/fonctions.inc.php');

//on indique que c'est pour une image en jpg
header("content-type:image/jpg");

//on définit la largeur redimensionné que l'on souhait
$larg_vign = 205;

//on récupère l'id de l'image
$id=(int)var_get('id');

//on définit le chemin d'accés à l'image
$chemin="data/images/$id.jpg";

//on récupère la taille de l'image H et L
$taille = getimagesize($chemin);


//calcul de la hauteur redimensionné
$haut_vign = $larg_vign*$taille[1]/$taille[0];

//on créé l'image source
$img_src = imagecreatefromjpeg($chemin);

//on créé l'image vignette
$img_vign = imagecreatetruecolor($larg_vign, $haut_vign);


//on redimensionne l'image source à la vignette
imagecopyresampled($img_vign, $img_src, 0, 0, 0, 0, $larg_vign, $haut_vign, $taille[0], $taille[1]);


//on renvois la vignette
imagejpeg($img_vign);



