<?php

//On inclus les fichiers necessaires
require_once('includes/connect.php');

require_once('includes/fonctions.inc.php');

//On récupère l'id de l'article
$id=(int)var_get('id');

//Si on a un id
if($id)
{
	//On selectionne l'image correspondant à l'article
	$image = "data/images/$id.jpg";

	//Si l'image existe on la supprime avec unlink()
	if(file_exists($image)) unlink($image);

	//On supprime l'article correspondant à l'id
	$sqlImg="DELETE FROM article WHERE id=$id";
	$res=mysql_query($sqlImg);

	request_notif($sqlImg,'article','supprimer');

	//Redirection vers index
	header('Location:index.php');


}