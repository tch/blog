<?php

//On inclut les fichiers necessaires
require_once('includes/connect.php');
require_once('includes/haut.inc.php');
require_once('includes/fonctions.inc.php');
require_once('includes/notifications.inc.php');


//On récupère l'id article
$id=(int)var_get('id');

//On test l'existance de l'id, si oui => requete SQL pour récupérer l'article correspondant dans la bdd
if($id != 0)
{
	$recupAr=mysql_query("SELECT * FROM article WHERE id=".$id);
	$recupTa=mysql_query("SELECT * FROM tags WHERE id_article=".$id);

	$dataTa=mysql_fetch_array($recupTa);
	$data=mysql_fetch_array($recupAr);

	$id_article = $id;

}


//Test si un ajout article a été envoyé
if (isset($_POST['article']))
{

	//On récupère et on affecte chaque champs à une variable
	$titre = var_post('titre');
	$texte = var_post('texte');
	$tag = var_post('tag');
	$image = var_post('image');

	
	//On test si tous les champs obligatoires sont remplis
	if($titre == false || $texte == false)
	{
		?>
<div class='alert alert-error'>

	<?php 
	echo 'Veuillez saisir tous les champs!'.$croix;
	//header('Refresh:1;url=article.php')
	?>
</div>
<?php 
	}else
	{
		
		//Si une image a été posté aussi, on test si il n'y a pas d'erreurs, la taille et le type
		if (isset($_POST['image']))
		{
		
			$erreur = $_FILES['image']['error'];
			$size = $_FILES['image']['size'];
			$type = $_FILES['image']['type'];
		
			//Test si pas d'erreur
			if($erreur != 0)
			{		
				echo "<div class='alert alert-error'>";
				echo "Erreur upload";
				echo "</div>";
			}
			//Test que la taille dépasse pas une certaine taille entré
			if($size > 2000000) 
			{
				echo "<div class='alert alert-error'>";
				echo "Erreur taille supérieur à 2 mo";
				echo "</div>";
		
			}
			//On test si c'est bien un type image/jpg
			if($type != "image/jpeg")
			{
				echo "<div class='alert alert-error'>";
				echo "Erreur format image";
				echo "</div>";
			}
		
		}
		//On sécurise les données de l'article
		$titre = mysql_real_escape_string($titre);
		$texte = mysql_real_escape_string($texte);
		$tag = mysql_real_escape_string($tag);
		$image = mysql_real_escape_string($image);

		//Si on a récupéré un id, alors on mets à jour l'article, et le tag correspondant à l'id
		if($id)
		{
			//MAJ article
			$sqlUpAr="UPDATE article SET titre='$titre',texte='$texte' WHERE id='$id'";
			
			//Si le champ tag est pas vide on met à jour le tag
			if($dataTa['tag'] !="")
			{
				$sqlUpTag="UPDATE tags SET tag='$tag' WHERE id_article='$id_article'";
			}
			//Sinon On supprime le tag correspondant à l'article vide
			else
			{
				$sqlUpTag="DELETE tag, id_article FROM tags WHERE id_article='$id_article'";
			}
			$resAr=mysql_query($sqlUpAr);
			$resTa=mysql_query($sqlUpTag);
			?>
<div class='alert alert-success'>
	<?php echo 'article a été modifié, vous allez être redirigé'.$croix;?>

</div>
<?php 



//	request_notif($sql,'article','modifier');
		}
		else{

			//On met time()+3600 pour correspondre au changement d'horaire (GMT+1)
			$time = time()+3600;
			
			//Si pas id, on insert un nouvel article dans la BDD
			$sqlIn = mysql_query("INSERT INTO article (titre,texte,date) VALUES ('$titre', '$texte','$time')");


			?>
<div class='alert alert-success'>

	<?php
	echo 'article a été ajouté, vous allez être rédirigé'.$croix;
	?>
</div>
<?php 

		}

		//On récupère le dernier id inséré
		$idf = mysql_insert_id();

		//On insère le tag correspondant à l'id de l'article
		$sqlTag = mysql_query("INSERT INTO tags (tag,id_article) VALUES ('$tag', '$idf')");

		
		//Si pas id, on bouge l'image du dossier temporaire => data/images, en renommant l'image avec l'id de l'article
		if($id==false)
		{
			move_uploaded_file($_FILES['image']['tmp_name'], "data/images/$idf.jpg");
			
		}

		echo mysql_error();
		
		//Refresh automatique au bout de 3 sec
		header('Refresh:3;url=index.php');


	}








	exit();


}


?>


<h2>
	<?php if($id) echo "Modifier article (indisponible pour l'image)"; else echo "Ajouter article"?>
</h2>

<form action="article.php?id=<?php echo $id;?>" method="post"
	enctype="multipart/form-data">

	<div class="input">
		<input type='hidden' name='article' value='1'>
	</div>



	<div class="clearfix">
		<label for="titre">Titre</label>
		<div class="input">
			<input type="text" name="titre" id="titre" 
			  value="<?php if($id) echo $data['titre'];?>">
		</div>
	</div>

	<div class="clearfix">
		<label for="texte">Texte</label>
		<div class="input">
			<textarea name="texte" id="texte"><?php if($id) echo $data['texte'];?></textarea>
		</div>
	</div>
	<!-- 
	<div class="clearfix">
		<label for="publie">Publié</label>
		<div class="input"><input type="checkbox" name="publie" id="publie" value="1"></div>
	</div>
	-->

	<div class="clearfix">
		<label for="tag">Tag</label>
		<div class="input">
			<input type="text" name="tag" id="tag"
				value="<?php if($id) echo $dataTa['tag'];?>">
		</div>
	</div>

	<?php if($id==false)
		echo '
		<div class="clearfix">
		<label for="image">Image</label>
		<div class="input"><input type="file" name="image"></div>
		</div>';
	?>
	<div class="form-actions">
		<input type="submit"
			value="<?php if($id) echo "Modifier"; else echo "Ajouter"?>"
			class="btn btn-large btn-primary">
	</div>


</form>
<script type="text/javascript" src="assets/js/fonction.js">
$(".cacher_notif").click(function(cacherNotif));
</script>
<?php
//On inclut bas 
require_once('includes/bas.inc.php');
?>