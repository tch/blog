-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Dim 13 Janvier 2013 à 20:47
-- Version du serveur: 5.5.27
-- Version de PHP: 5.4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'id de l''article',
  `titre` varchar(255) NOT NULL COMMENT 'titre article',
  `texte` text NOT NULL COMMENT 'contenu article',
  `date` int(11) NOT NULL COMMENT 'date de l''article',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `texte`, `date`) VALUES
(63, 'SNSD', 'wallpaper du groupe SNSD', 1357751616),
(64, 'nozomi', 'nozomi sasaki j-doll', 1357843685),
(65, 'nozomi', 'nozomi j-doll 2', 1357843768),
(66, 'nozomi', 'wallpaper 3 de nozomi sasaki, j-doll', 1357900363),
(67, 'yoona', 'une des 9 chanteuses du groupe SNSD', 1357911587),
(68, 'Leah dizon', 'la jolie Leah Dizon J-doll', 1357913586),
(69, 'Miku Hatstune', 'Wallpaper de Hatstune Miku, chanteuse très populaire au japon.', 1357913694),
(70, 'Girl''s generation', 'Sunny -  Taeyeon - Hyoyeon  du groupe k-pop Girl''s generation, alias SNSD						', 1358112430);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag` varchar(50) NOT NULL COMMENT 'tag en lui meme',
  `id_article` int(5) NOT NULL COMMENT 'id_article correspondant'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`tag`, `id_article`) VALUES
('SNSD', 63),
('nozomi', 64),
('nozomi', 65),
('nozomi', 66),
('yoona', 67),
('', 0),
('J-doll', 68),
('', 69),
('miku', 0),
('Miku', 0),
('miku', 0),
('', 0),
('J-doll', 0),
('SNSD', 70);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id utilisateur',
  `email` varchar(100) NOT NULL COMMENT 'email de l''utilisateur',
  `mdp` varchar(30) NOT NULL COMMENT 'mot de passe',
  `SID` varchar(255) NOT NULL COMMENT 'SID de l''user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `email`, `mdp`, `SID`) VALUES
(1, 'testemail@mail.com', 'juju', '6a5316f369cebf976b17fec905d4ab1c'),
(2, 'testemail2@mail.com', 'jiji', 'c190f655b8d00cb8d57a2c2180b4ab05'),
(3, 'jlannoy@mail.com', 'nilsine', '7f324a0bdbb11fac7800cc6bf15e4c49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
