<?php /* Smarty version Smarty-3.1.12, created on 2013-01-15 13:42:56
         compiled from "templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:181780004850be0670020ff1-44407294%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90093ad09988b466f409a1871733c5589014713e' => 
    array (
      0 => 'templates/index.tpl',
      1 => 1358257372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '181780004850be0670020ff1-44407294',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_50be0670049b47_20489420',
  'variables' => 
  array (
    'page' => 0,
    'precedent' => 0,
    'max' => 0,
    'i' => 0,
    'suivant' => 0,
    'titre' => 0,
    'article' => 0,
    'articles' => 0,
    'config' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50be0670049b47_20489420')) {function content_50be0670049b47_20489420($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/workspace/blog/libs/plugins/modifier.date_format.php';
?><html>

<div class="pagination">

<ul>
	
		<li class="prev <?php if ($_smarty_tpl->tpl_vars['page']->value<=1){?> disabled <?php }?>"><a href="?p=<?php if ($_smarty_tpl->tpl_vars['page']->value<=1){?> #null <?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['precedent']->value;?>
<?php }?>">&larr; Precedent</a></li>
		
		<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['max']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['max']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
 			<li <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['i']->value){?> class='active' <?php }?>>
 			<a href='?p=<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
'><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</a></li>
			
		<?php }} ?>
		<li class="next <?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['max']->value){?> disabled <?php }?>"><a href="?p=<?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['max']->value){?><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['suivant']->value;?>
<?php }?>">Suivant&rarr;</a></li>
</ul>
		
</div>

<h2><?php echo $_smarty_tpl->tpl_vars['titre']->value;?>
<h2>
	
<?php  $_smarty_tpl->tpl_vars['articles'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['articles']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['article']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['articles']->key => $_smarty_tpl->tpl_vars['articles']->value){
$_smarty_tpl->tpl_vars['articles']->_loop = true;
?>
	<h3><?php echo $_smarty_tpl->tpl_vars['articles']->value['titre'];?>
</h3>	
	<p class="article">
	<?php if ($_smarty_tpl->tpl_vars['articles']->value['image']){?>
		
		<a href="data/images/<?php echo $_smarty_tpl->tpl_vars['articles']->value['id'];?>
.jpg"><img src="vignette.jpg.php?id=<?php echo $_smarty_tpl->tpl_vars['articles']->value['id'];?>
"></a>
	
		
	<?php }?>
	
	<br/>
	<br/>
	<span><?php echo nl2br($_smarty_tpl->tpl_vars['articles']->value['texte']);?>
</span>
	<br/>
	
	<br/>
	publié le :
	<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['articles']->value['date'],"%A %d-%B-%Y");?>
 à
	<br/>
	<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['articles']->value['date'],$_smarty_tpl->tpl_vars['config']->value['date']);?>

	<br/>
	
	</p>
	<div class="tag">
	Tag : <a href="?tag=<?php echo $_smarty_tpl->tpl_vars['articles']->value['tag'];?>
"><?php echo $_smarty_tpl->tpl_vars['articles']->value['tag'];?>
</a>
	</div>
	<br/>
	<?php if ($_smarty_tpl->tpl_vars['user']->value==true){?>
<a href="article.php?id=<?php echo $_smarty_tpl->tpl_vars['articles']->value['id'];?>
" class="btn btn-primary">Modifier</a>
<a href="SuppArticle.php?id=<?php echo $_smarty_tpl->tpl_vars['articles']->value['id'];?>
" class="btn btn-danger">Supprimer</a>
	<?php }?>
<?php } ?>

<div class="pagination">

<ul>
	
		<li class="prev <?php if ($_smarty_tpl->tpl_vars['page']->value<=1){?> disabled <?php }?>"><a href="?p=<?php if ($_smarty_tpl->tpl_vars['page']->value<=1){?> #null <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['precedent']->value;?>
<?php }?>">&larr; Precedent</a></li>
		
		<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['max']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['max']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
 			<li <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['i']->value){?> class='active' <?php }?>>
			<a href='?p=<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
'><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</a></li>
		<?php }} ?>
		<li class="next <?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['max']->value){?> disabled <?php }?>"><a href="?p=<?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['max']->value){?> <?php echo $_smarty_tpl->tpl_vars['page']->value;?>
 <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['suivant']->value;?>
<?php }?>">Suivant&rarr;</a></li>
</ul>
		
</div>



</html><?php }} ?>