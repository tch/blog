<?php
//On inclut les fichiers necessaires
include('includes/notifications.inc.php');
include('includes/connect.php');
require_once('includes/fonctions.inc.php');


//On recrée le cookie email, sid et on les met à 0 afin que le navigateur supprime les cookies
setcookie('email','','1');
setcookie('sid','','1');

//On remet l'utilisateur à faux
$user==false;

//Redirection vers index
Header("Location:index.php");